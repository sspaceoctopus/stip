/**
 * Created by DELL on 05.04.2018.
 */
"use strict";

(function () {
    var labNumsList = document.querySelector('#labNumsList');
    var onLabNumClick = function (evt) {
        if (evt.target.tagName == 'LI' ){
            var labNumSpan = document.querySelector('#labNumber');
            var labTheme = document.querySelector('#labTheme');
            var labLink = document.querySelector('#labLink');
            var labNum = parseInt(evt.target.dataset.lab);
            labNumSpan.textContent = labNum;
            switch (labNum) {
                case 1 :
                    labTheme.textContent = 'Тема: Создание web-страниц на основе HTML. Возможности языка. Инструменты разработки программ.';
                    labLink.href ='l1/index.html';
                    break;
                case 2 :
                    labTheme.textContent = 'Тема: Форматування web документов за допомогою каскадних таблиць стилів CSS (Cascading Style Sheets)';
                    labLink.href ='l2/index.html';
                    break;
                case 3 :
                    labTheme.textContent = 'Тема: Сценарии JavaScript. Реализация программного взаимодействия JavaScript с HTML документами на основе DOM.';
                    labLink.href ='l3/index.html';
                    break;
                case 4 :
                    labTheme.textContent = 'Тема: Сценарии JavaScript. Реализация программного взаимодействия JavaScript с HTML документами на основе DOM.';
                    labLink.href ='l4/index.html';
                    break;
                case 5 :
                    labTheme.textContent = 'Тема: ОБРАБОТКА РЕГУЛЯРНЫХ ВЫРАЖЕНИЙ С ПОМОЩЬЮ JAVASCRIPT"';
                    labLink.href ='l5/index.html';
                    break;
                case 6 :
                    labTheme.textContent = 'Тема: Модель HTML DOM';
                    labLink.href ='l6/index.html';
                    break;
                case 7 :
                    labTheme.textContent = 'Тема: Работа со стандартными объектами JAVASCRIPT';
                    labLink.href ='l7/index.html';
                    break;
                case 8 :
                    labTheme.textContent = 'Тема: Работа со слоями и изображениями';
                    labLink.href ='l8/index.html';
                    break;
                case 9 :
                    labTheme.textContent = 'Тема: Работа с JavaScript Framework jQuery';
                    labLink.href ='l9/index.html';
                    break;
                case 10 :
                    labTheme.textContent = 'Тема: Создание XML-документа';
                    labLink.href ='l10/index.html';
                    break;
                case 11 :
                    labTheme.textContent = 'Тема: Описание схемы XML-документа на языке XSD';
                    labLink.href ='l11/index.html';
                    break;
                case 12 :
                    labTheme.textContent = 'Тема: Форматирование вывода XML-документа';
                    labLink.href ='l12/index.html';
                    break;
            }
        }
    };
    labNumsList.addEventListener('click',onLabNumClick);

})();