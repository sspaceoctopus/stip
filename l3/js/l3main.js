'use strict';

var calculateA = function () {
    var rad = parseFloat(document.getElementById("rad").value);
    var fi = parseFloat(document.getElementById("fi").value);
    if (/^\d+$/.test(rad) && /^\d+$/.test(fi)) {
        document.getElementById("ploshad").innerHTML = (Math.pow(rad, 2) * fi) / 2 + " см <sup>2</sup>";
        document.getElementById("obem").innerHTML = (4 / 3 * Math.PI * Math.pow(rad, 3)).toFixed(2) + " см";
    }
    else {
        alert("Попробуйте ввести числа");
    }
};

var calculateB = function () {
    var a = parseFloat(document.getElementById("A").value);
    var b = parseFloat(document.getElementById("B").value);
    var c = parseFloat(document.getElementById("C").value);
    if (a + b <= c || a + c <= b || b + c <= a) alert("Невозможно построить треугольник по заданным сторонам. Введите другие значения.");
    else if (/^\d+$/.test(a) && /^\d+$/.test(b) && /^\d+$/.test(c)) {
        document.getElementById("bissA").innerHTML = (Math.sqrt(c * b * (a + b + c) * (c + b - a)) / (c + b)).toFixed(2) ;
        document.getElementById("bissB").innerHTML = (Math.sqrt(a * c * (a + b + c) * (a + c - b)) / (a + c)).toFixed(2);
        document.getElementById("bissC").innerHTML = (Math.sqrt(a * b * (a + b + c) * (a + b - c)) / (a + b)).toFixed(2);
    }
    else {
        alert("Попробуйте ввести числа");
    }
};

