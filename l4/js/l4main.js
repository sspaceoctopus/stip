(function () {
    'use strict';
    var nums = document.getElementsByClassName('num');
    var actions = document.getElementsByClassName('action');
    var reset = document.getElementById('reset');
    var result = document.getElementById('result');
    var numsAndActionsWrapper = document.querySelector('.num-and-action');

    numsAndActionsWrapper.addEventListener('click',function (e) {
        if (e.target.innerHTML !== "="){
            if (/[\D.]+/.test(result.innerHTML.substr(-1))&& /[\D.]+/.test(e.target.innerHTML)) {
                var replaceVal = e.target.innerHTML.toString();
                result.innerHTML = result.innerHTML.replace(/.$/,replaceVal);
            }
            else {
                result.innerHTML += e.target.innerHTML;
            }
        }
        else {
            if(result.innerHTML) {
                result.innerHTML = eval(result.innerHTML);
            }
        }
    });
    reset.onclick = function () {
        result.innerHTML = "";
    };

})();
